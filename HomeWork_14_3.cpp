﻿#include <iostream>
#include <string>

int main()
{
    std::string variable = "Roma ragimov";
    std::cout << variable << "\n";
    std::cout << "Length :" << " " <<variable.length() << "\n";
    std::cout << "first simbol :" << " " << variable[0] << "\n";
    std::cout << "Last symbol :" << " " << variable[variable.size()-1] << "\n";
    return 0;
}
